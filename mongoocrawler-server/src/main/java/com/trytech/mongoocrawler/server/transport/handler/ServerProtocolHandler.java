package com.trytech.mongoocrawler.server.transport.handler;

import com.trytech.mongoocrawler.common.transport.protocol.ProtocolHandler;
import com.trytech.mongoocrawler.server.CrawlerContext;

/**
 * @author jiangtao@meituan.com
 * @since: Created on 2018年07月01日
 */
public abstract class ServerProtocolHandler extends ProtocolHandler {
    protected CrawlerContext crawlerContext;

    public ServerProtocolHandler(CrawlerContext crawlerContext) {
        this.crawlerContext = crawlerContext;
    }

}
