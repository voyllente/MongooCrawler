package com.trytech.mongoocrawler.server.transport;

import com.trytech.mongoocrawler.server.CrawlerSession;
import com.trytech.mongoocrawler.server.common.queue.WebFetcherEventProducer;
import com.trytech.mongoocrawler.server.transport.http.CrawlerHttpRequest;
import com.trytech.mongoocrawler.server.transport.http.HttpClient;
import com.trytech.mongoocrawler.server.transport.http.WebResult;

/**
 * Created by hp on 2017-1-24.
 */
public class TaskWorker implements Runnable {
    private int REQUESTS_MAX_COUNT=10;//最多尝试请求10次，否则丢弃请求
    private CrawlerHttpRequest request;
    private CrawlerSession session;
    private WebFetcherEventProducer webFetcherEventProducer;

    public TaskWorker(CrawlerHttpRequest request, CrawlerSession session){
        this.request = request;
        this.session = session;
        this.webFetcherEventProducer = new WebFetcherEventProducer(session.getDisruptorContext().getWebResultRingBuffer());
    }

    public CrawlerSession getSession(){
        return session;
    }
    @Override
    public void run() {
            for(int i=0;i<=REQUESTS_MAX_COUNT;i++) {
                try {
                    WebResult<String> result = HttpClient.doHtmlGet(request);
                    result.setParser(request.getParser());
                    result.setUrl(request.getUrl().toString());
                    WebResult.StatusCode statuscode = result.getStatuscode();
                    System.out.println("正在获取："+result.getUrl()+"的内容；状态码："+statuscode.getStatuscode());
                    if (statuscode.equals(WebResult.StatusCode.SUCCESS)) {
                        //将网页发送到disruptor中
                        webFetcherEventProducer.sendData(result);
                        break;
                    }
                    if (i == REQUESTS_MAX_COUNT) {
                        //丢弃请求并打印日志

                    }
                }catch (Exception e){

                }
            }
    }
}
