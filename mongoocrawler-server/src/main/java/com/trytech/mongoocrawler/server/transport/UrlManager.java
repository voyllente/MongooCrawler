package com.trytech.mongoocrawler.server.transport;

import com.trytech.mongoocrawler.server.CryptUtils;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * redis内存队列
 */
public abstract class UrlManager {
    protected LinkedBlockingDeque<UrlParserPair> urlQueue = new LinkedBlockingDeque<UrlParserPair>();
    //保存不同url的md5码
    private HashMap<String,Object> md5OfUrlMap = new LinkedHashMap<String,Object>();

    public abstract void pushUrl(UrlParserPair urlParserPair);

    public abstract void pushUrls(UrlParserPair[] urlParserPairs);

    public abstract UrlParserPair fetchUrl();
    /***
     * url去重，使用MD5算法，允许一定的错误率
     * @param url
     * @return
     */
    public boolean isUnique(String url){
        String md5OfUrl = CryptUtils.encryptByMD5(url);
        if(md5OfUrlMap.containsKey(md5OfUrl)){
            return false;
        }
        md5OfUrlMap.put(md5OfUrl, new Object());
        return true;
    }

    public boolean isQueueEmpty() {
        return urlQueue.isEmpty();
    }
}
