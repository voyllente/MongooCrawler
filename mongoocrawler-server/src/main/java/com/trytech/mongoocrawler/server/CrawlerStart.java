package com.trytech.mongoocrawler.server;

/**
 * 爬虫启动类
 */
public class CrawlerStart {
    public static void main(String args[]) {
        CrawlerContext crawlerContext = CrawlerContext.getCrawlerContext();
        crawlerContext.start();
    }
}
