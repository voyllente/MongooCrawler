package com.trytech.mongoocrawler.common.bean;

import com.trytech.mongoocrawler.common.util.DateUtils;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.NoArgsConstructor;

/**
 * Created by coliza on 2018/5/24.
 */
@NoArgsConstructor
public class ClientInfo {
    //ip地址
    private String ip;
    private ClientStatusEnum statusEnum;
    private String connectTime;
    private NioSocketChannel socketChannel;

    public ClientInfo(String ip, NioSocketChannel socketChannel) {
        this.ip = ip;
        this.statusEnum = ClientStatusEnum.FREE;
        this.connectTime = DateUtils.now();
        this.socketChannel = socketChannel;

    }

    enum ClientStatusEnum {
        FREE(1), RUNNING(2);
        private int status;

        ClientStatusEnum(int status) {
            this.status = status;
        }

        public int getStatus() {
            return this.status;
        }

        public boolean equalsTo(ClientStatusEnum to) {
            return this.status == to.getStatus();
        }
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public ClientStatusEnum getStatusEnum() {
        return statusEnum;
    }

    public void setStatusEnum(ClientStatusEnum statusEnum) {
        this.statusEnum = statusEnum;
    }

    public String getConnectTime() {
        return connectTime;
    }

    public void setConnectTime(String connectTime) {
        this.connectTime = connectTime;
    }

    public NioSocketChannel getSocketChannel() {
        return socketChannel;
    }

    public void setSocketChannel(NioSocketChannel socketChannel) {
        this.socketChannel = socketChannel;
    }
}
