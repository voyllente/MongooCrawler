package com.trytech.mongoocrawler.common.transport.protocol;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * @author jiangtao@meituan.com
 * @since: Created on 2018年04月11日
 */
public class ProtocolEncoder extends MessageToByteEncoder<CrawlerTransferProtocol> implements Protocol {


    @Override
    protected void encode(ChannelHandlerContext ctx, CrawlerTransferProtocol msg, ByteBuf out) throws Exception {
        out.writeBytes(MAGIC);
        out.writeByte(msg.toTypeByte());
        byte[] nameBytes = msg.getCls().getName().getBytes();
        out.writeInt(nameBytes.length);
        out.writeBytes(nameBytes);
        out.writeInt(msg.getContentLen());
        out.writeBytes(msg.toContentBytes());
    }
}
