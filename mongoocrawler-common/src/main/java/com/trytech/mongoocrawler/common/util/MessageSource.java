package com.trytech.mongoocrawler.common.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by hp on 2017-1-24.
 */
public class MessageSource {
    private static MessageSource instance;
    private Properties properties;

    private MessageSource() {
    }

    public static MessageSource getInstance() {
        if (instance == null) {
            instance = new MessageSource();
            try {
                Properties newProperties = new Properties();
                FileInputStream in = new FileInputStream(System.getProperty("user.dir") + "/messages.properties");
                newProperties.load(in);
                in.close();
                instance.setProperties(newProperties);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return instance;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    public String getMessage(String key) {
        return this.properties.getProperty(key);
    }
}
