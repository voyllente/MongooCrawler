package com.trytech.mongoocrawler.web.vo;

import lombok.Data;

/**
 * Created by coliza on 2018/6/2.
 */
@Data
public class ServerConfVO {
    private int mode;
    private String modeLabel;
    private int crawlerCount;
    private String runStatus;

}
