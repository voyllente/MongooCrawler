package com.trytech.mongoocrawler.client;

import com.trytech.mongoocrawler.common.transport.protocol.AbstractProtocol;
import com.trytech.mongoocrawler.common.transport.protocol.CommandProtocol;
import com.trytech.mongoocrawler.common.transport.protocol.ProtocolFilterChain;
import com.trytech.mongoocrawler.common.transport.protocol.ProtocolHandler;

/**
 * 命令协议解析器
 *
 * @author jiangtao@meituan.com
 * @since: Created on 2018年04月10日
 */
public class CommandProtocolHandler extends ProtocolHandler {
    @Override
    public AbstractProtocol doHandle(ProtocolFilterChain filterChain, AbstractProtocol abstractProtocol) throws Exception {
        CommandProtocol commandProtocol = (CommandProtocol) abstractProtocol;
        if (commandProtocol != null && commandProtocol.getCommand() != null) {
            CommandProtocol.Command command = commandProtocol.getCommand();
            if (command.equalsTo(CommandProtocol.Command.CONTINUE)) {
                commandProtocol.setCommand(CommandProtocol.Command.GET_URL);
                commandProtocol.setTraceId("FEDK1209J1JD123");
                return commandProtocol;
            }

        }
        return filterChain.doFilter(abstractProtocol);
    }

    @Override
    protected boolean checkProtocol(AbstractProtocol abstractProtocol) {
        return abstractProtocol instanceof CommandProtocol;
    }
}
