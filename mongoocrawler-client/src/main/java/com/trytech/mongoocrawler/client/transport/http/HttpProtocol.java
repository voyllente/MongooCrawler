package com.trytech.mongoocrawler.client.transport.http;

/**
 * Created by coliza on 2017/7/30.
 */
public abstract class HttpProtocol {
    public final static String ENCODING ="utf-8";
    protected HttpBody body;

    public HttpBody getBody() {
        return body;
    }

    public void setBody(HttpBody body) {
        this.body = body;
    }

    //请求方法
    public static enum METHOD{
        POST("post"),GET("get");
        private String value;
        METHOD(String value){
            this.value = value;
        }
        public String value(){
            return this.value;
        }
        public boolean equals(METHOD another){
            return value.equalsIgnoreCase(another.value());
        }
    }
}
