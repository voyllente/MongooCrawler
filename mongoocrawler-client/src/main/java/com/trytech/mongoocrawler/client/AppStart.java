package com.trytech.mongoocrawler.client;

/**
 * @author jiangtao@meituan.com
 * @since: Created on 2018年04月10日
 */
public class AppStart {

    public static void main(String[] args){
        CrawlerContext crawlerContext = CrawlerContext.newInstance();
        crawlerContext.start();
    }
}
